// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'http://localhost:4200/api',
  httpParams: {
    pageLimit: '_page',
    limit: '_limit',
    searchType: 'q'
  },
  numberOfRandomProductsOnHomePage: 3,
  randomProductsInterval: 10000,
  oauthServiceConfig: {
    debugInfo: true,
    issuer: 'https://AUTH0-DOMAIN',
    redirectUri: 'http://localhost:4200/home',
    logoutUrl: 'https://AUTH0-DOMAIN/v2/logout',
    revocationEndpoint: 'https://AUTH0-DOMAIN/oauth/revoke',
    clientId: 'CODE_ID'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
