import { Component, OnInit } from '@angular/core';
import { ProductService } from '../core/services/product.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-admin-product-add',
  templateUrl: './admin-product-add.component.html',
  styleUrls: ['./admin-product-add.component.scss']
})
export class AdminProductAddComponent implements OnInit {

  constructor(private productsService: ProductService) { }

  ngOnInit(): void {
  }

  add(product: Product | undefined): void{
    if(product){
      this.productsService.addProduct(product);
    }
  }

}
