import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../core/services/product.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-admin-product-edit',
  templateUrl: './admin-product-edit.component.html',
  styleUrls: ['./admin-product-edit.component.scss']
})
export class AdminProductEditComponent implements OnInit {

  product: Product | undefined;

  constructor(private productsService: ProductService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.productsService.getProduct(this.getProductIdFromRouteSnapshot()).subscribe(
      ((data: Product) => this.product = { ...data})
    );
  }

  getProductIdFromRouteSnapshot(): number{
    return Number(this.route.snapshot.paramMap.get('id'));
  }

  update(product: Product | undefined): void{
    if(product){
      this.productsService.updateProduct(product);
    }
  }
}
