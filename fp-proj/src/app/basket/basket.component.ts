import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { BasketService } from '../core/services/basket.service';
import { BasketItem } from '../models/BasketItem';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {

  readonly basketItems$: Observable<BasketItem[]> = this.basketService.basketItems$.pipe(shareReplay());

  readonly totalPrice$: Observable<number> = this.basketService.totalPrice$.pipe(shareReplay());

  isPurchaseCompleted: boolean = false;

  constructor(private basketService: BasketService) {
  }

  ngOnInit(): void {
    this.basketService.getTotalPrice();
    this.basketService.fetchBasketItems();
  }

  deleteItem(productId: number | undefined){
    if(productId){
      this.basketService.removeBasketItem(productId);
    }
  }

  completePurchase(){
    this.isPurchaseCompleted = true;
    this.basketService.clearBasket();
  }

  clearBasket(){
    this.basketService.clearBasket();
    this.isPurchaseCompleted = false;
  }
}
