import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/authentication.service';
import { BasketService } from '../core/services/basket.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private basketService: BasketService) { }

  readonly isAuthenticated$: Observable<boolean> = this.authenticationService.isAuthenticated$.pipe(shareReplay());

  ngOnInit(): void {
  }

  login(): void{
    this.authenticationService.login();
  }

  logout(): void{
    this.authenticationService.logout();
  }

  getUserName(){
    return this.authenticationService.givenName;
  }

  getNumberOfItemsInBasket(): number{
    return this.basketService.getNumberOfItemsInBasket();
  }
}
