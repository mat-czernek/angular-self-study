import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminDahsboardComponent } from '../admin-dahsboard/admin-dahsboard.component';
import { AdminMessagesComponent } from '../admin-messages/admin-messages.component';
import { AdminProductAddComponent } from '../admin-product-add/admin-product-add.component';
import { AdminProductEditComponent } from '../admin-product-edit/admin-product-edit.component';
import { AuthGuard } from '../auth-guard.guard';

const routes: Routes = [
  { path: '', component: AdminDahsboardComponent, canActivate: [AuthGuard] },
  { path: 'messages', component: AdminMessagesComponent, canActivate: [AuthGuard] },
  { path: 'products/edit/:id', component: AdminProductEditComponent, canActivate: [AuthGuard] },
  { path: 'products/add', component: AdminProductAddComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AdminRoutingModule { }
