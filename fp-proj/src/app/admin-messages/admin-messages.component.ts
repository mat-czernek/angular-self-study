import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MessagesService } from '../core/services/messages.service';
import { UserMessage } from '../models/UserMessage';

@Component({
  selector: 'app-admin-messages',
  templateUrl: './admin-messages.component.html',
  styleUrls: ['./admin-messages.component.scss']
})
export class AdminMessagesComponent implements OnInit {

  readonly messages$: Observable<UserMessage[]> = this.messagesService.messages$.pipe(shareReplay());

  public activeMessage: string | undefined;

  public areMessagesAvailable: boolean | undefined;

  constructor(private messagesService: MessagesService) { }

  ngOnInit(): void {
    this.messagesService.fetchMessages();

    this.messages$.subscribe(data => {
      if(data.length > 0){
        this.areMessagesAvailable = true;
      }
      else{
        this.areMessagesAvailable = false;
      }
    })
  }

  public showMessage(message:string){
    this.activeMessage = message;
  }
}
