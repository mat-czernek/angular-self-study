import { Component, EventEmitter, Input, OnInit, Output, SimpleChange, SimpleChanges } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../models/Product';

@Component({
  selector: 'app-admin-product-form',
  templateUrl: './admin-product-form.component.html',
  styleUrls: ['./admin-product-form.component.scss']
})
export class AdminProductFormComponent implements OnInit {

  @Input() product: Product | undefined;
  @Output() saveProduct = new EventEmitter<Product>();

  productForm = new FormGroup({
    name: new FormControl('', [Validators.maxLength(60), Validators.required]),
    description: new FormControl('', [Validators.maxLength(200), Validators.required]),
    price: new FormControl('', [Validators.required, Validators.min(0), Validators.pattern("^\\d+\\.\\d{0,2}$")]),
    stockQuantity: new FormControl('', [Validators.required, Validators.min(0)])
  });

  constructor(private activeRoute: ActivatedRoute, private router: Router) { }

  get name() { return this.productForm.get('name')};

  get description() { return this.productForm.get('description')};

  get price() { return this.productForm.get('price')};

  get stockQuantity() { return this.productForm.get('stockQuantity')};

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void{
    if(changes.product.currentValue){

      let loadedProduct = changes.product.currentValue;

      this.productForm.patchValue({
        description: loadedProduct.description,
        name: loadedProduct.name,
        price: loadedProduct.price,
        stockQuantity: loadedProduct.stockQuantity
      });
    }
  }

  save(): void{

    if(this.productForm.valid){
      const product: Product = { id: this.getProductIdFromRouteSnapshot(), name: this?.name?.value, description: this?.description?.value, price: this?.price?.value, stockQuantity: this?.stockQuantity?.value};

      this.saveProduct.emit(product);

      this.router.navigate(['/products']);
    }
  }

  getProductIdFromRouteSnapshot(): number{
    return Number(this.activeRoute.snapshot.paramMap.get('id'));
  }
}
