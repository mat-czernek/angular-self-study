import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BasketComponent } from './basket/basket.component';
import { AdminDahsboardComponent } from './admin-dahsboard/admin-dahsboard.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'home/:id', redirectTo: '/products/:id', pathMatch: 'full' },
  { path: 'products', loadChildren: () => import('./core/products.module').then(m => m.ProductsModule) },
  { path: 'contact', component: ContactComponent },
  { path: 'basket', component: BasketComponent },
  { path: 'admin', loadChildren: () => import('./core/admin.module').then(m => m.AdminModule) },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
