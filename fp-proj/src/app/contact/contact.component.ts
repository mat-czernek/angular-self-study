import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ContactService } from '../core/services/contact.service';
import { UserMessage } from '../models/UserMessage';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  isMessageSend: boolean = false;

  contactForm = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    message: new FormControl('', [Validators.maxLength(200), Validators.required])
  });

  get email() { return this.contactForm.get('email')};

  get message() { return this.contactForm.get('message')};

  constructor(private contactService: ContactService) { }

  ngOnInit(): void {
  }

  sendMessage(): void {
    if(this.contactForm.valid){

      const userMessage: UserMessage = { email: this?.email?.value, message: this?.message?.value};

      this.contactService.send(userMessage);

      this.isMessageSend = true;
    }
  }

}
