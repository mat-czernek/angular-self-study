export interface UserMessage{
  id?: number;
  email: string;
  message: string;
}
