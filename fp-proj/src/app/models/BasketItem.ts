export interface BasketItem{
  productId?: number;
  count: number;
  name: string;
  price: number;
}
