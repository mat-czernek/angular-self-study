import { Component, OnInit } from '@angular/core';
import { PageChangedEvent } from 'ngx-bootstrap/pagination';
import { Observable, Subscription } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/authentication.service';
import { BasketService } from '../core/services/basket.service';
import { ProductService } from '../core/services/product.service';
import { SharedService } from '../core/services/shared.service';
import { Product } from '../models/Product';


@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  readonly isAuthenticated$: Observable<boolean> = this.authenticationService.isAuthenticated$.pipe(shareReplay());

  private _isFilteringInProgress: boolean = false;
  private _productsFilteringTerm: string = '';

  readonly productsPerPage: number = 4;
  readonly products$: Observable<Product[]> = this.productService.products$.pipe(shareReplay());
  readonly totalProducts$: Observable<number> = this.productService.totalProducts$.pipe(shareReplay());

  constructor(
    private productService: ProductService,
    private authenticationService: AuthenticationService,
    public sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.productService.fetchProducts();
  }

  isFilteringInProgress(isInProgress: boolean): void{
    if(!isInProgress){
      this.productService.fetchProducts();
    }

    this._isFilteringInProgress = isInProgress;
  }

  filterProducts(filteringTerm: string): void {
    this._productsFilteringTerm = filteringTerm;
    this.productService.filterProducts(filteringTerm);
  }

  pageChanged(event: PageChangedEvent): void {
    if(!this._isFilteringInProgress){
      this.productService.fetchProducts(event.page, event.itemsPerPage);
    }
    else{
      this.productService.filterProducts(this._productsFilteringTerm,event.page, event.itemsPerPage);
    }
  }
}
