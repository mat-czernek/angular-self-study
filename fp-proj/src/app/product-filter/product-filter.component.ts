import { Component, ElementRef, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { debounceTime, map, distinctUntilChanged } from "rxjs/operators";
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-product-filter',
  templateUrl: './product-filter.component.html',
  styleUrls: ['./product-filter.component.scss']
})


export class ProductFilterComponent implements OnInit {

  @Output() filterProductsEvent = new EventEmitter<string>();
  @Output() isFilteringInProgressEvent = new EventEmitter<boolean>();
  @ViewChild('productSearchInput', { static: true }) productSearchInput: ElementRef | undefined;

  constructor() {}

  ngOnInit(): void {
    fromEvent(this.productSearchInput?.nativeElement, 'keyup').pipe(

      map((event: any) => {
        return event.target.value;
      })

      ,debounceTime(400)

      ,distinctUntilChanged()

    ).subscribe((filterTerm: string) => {

      if(filterTerm.length !== 0){
        this.isFilteringInProgressEvent.emit(true);
        this.filterProductsEvent.emit(filterTerm);
      }
      else{
        this.isFilteringInProgressEvent.emit(false);
      }
    });
  }
}
