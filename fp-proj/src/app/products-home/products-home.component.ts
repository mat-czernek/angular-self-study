import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ProductService } from '../core/services/product.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-products-home',
  templateUrl: './products-home.component.html',
  styleUrls: ['./products-home.component.scss']
})
export class ProductsHomeComponent implements OnInit {

  readonly randomProducts$: Observable<Product[]> = this.productService.randomProducts$.pipe(shareReplay());

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
    this.productService.fetchRandomProducts();
  }

}
