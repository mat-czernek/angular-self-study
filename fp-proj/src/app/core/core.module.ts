import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ProductsModule } from './products.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactComponent } from '../contact/contact.component';
import { SharedModule } from './shared.module';
import { PaginationModule } from 'ngx-bootstrap/pagination'
import { OAuthModule } from 'angular-oauth2-oidc';
import { AdminModule } from './admin.module';

@NgModule({
  declarations: [
    ContactComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ProductsModule,
    AdminModule,
    ReactiveFormsModule,
    SharedModule,
    PaginationModule.forRoot(),
    OAuthModule.forRoot()
  ]
})
export class CoreModule { }
