import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { BasketItem } from 'src/app/models/BasketItem';
import { Product } from 'src/app/models/Product';

@Injectable({
  providedIn: 'root'
})
export class BasketService {

  items: BasketItem[];

  private totalPrice: BehaviorSubject<number> = new BehaviorSubject<number>(0);
  public totalPrice$ = this.totalPrice.asObservable();

  private basketItems: BehaviorSubject<BasketItem[]> = new BehaviorSubject<BasketItem[]>([]);
  public basketItems$ = this.basketItems.asObservable();

  constructor() {
    this.items = new Array<BasketItem>();

    this.getItemsFromStorage();

    this.basketItems.subscribe(() => {
      this.totalPrice.next(this.getTotalPrice());
    })
  }

  fetchBasketItems(){
    this.basketItems.next(this.items);
  }

  getNumberOfItemsInBasket(): number{
    return this.items.reduce((p, { count }) => p + count, 0);
  }

  getTotalPrice(): number{

    let totalPrice: number = 0;

    this.items.forEach( (item) => {
      totalPrice += item.price * item.count;
    });

    this.totalPrice.next(totalPrice);

    return totalPrice;
  }

  insertBasketItem(product: Product): void{
    let existingItem = this.items.find(i => i.productId === product.id);

    if(existingItem){
      this.updateExistingItem(existingItem);
      return;
    }

    this.addNewItem(product);
  }

  removeBasketItem(productId: number){
    let existingItem = this.items.find(i => i.productId === productId);

    if(existingItem){

      if(existingItem.count == 1){
        this.deleteItem(existingItem);
        return;
      }

      this.decrementItemQuantity(existingItem);
      return;
    }
  }

  clearBasket(): void{
    localStorage.removeItem('basketItems');
    this.basketItems.next([]);
    this.totalPrice.next(0);
    this.items = [];
  }

  getProductsCountById(productId: number | undefined): number{
    let existingItem = this.items.find(i => i.productId === productId);

    if(!existingItem){
      return 0;
    }

    return existingItem.count;
  }

  private getItemsFromStorage(){
    var itemsFromStorage = localStorage.getItem('basketItems');

    if(itemsFromStorage){
      this.items = JSON.parse(itemsFromStorage);
    }
  }

  private updateExistingItem(basketItem: BasketItem){
    let itemIndex = this.items.indexOf(basketItem);
    basketItem.count = basketItem.count + 1;
    this.items[itemIndex] = basketItem;

    this.updateBasket();
  }

  private addNewItem(product: Product){
    const newItem: BasketItem = { productId: product.id, count: 1, name: product.name, price: product.price  };

    this.items.push(newItem)

    this.updateBasket();
  }

  private deleteItem(basketItem: BasketItem){
    let itemIndex = this.items.indexOf(basketItem);
    this.items.splice(itemIndex, 1);

    this.updateBasket();
  }

  private decrementItemQuantity(baseketItem: BasketItem){
    let itemIndex = this.items.indexOf(baseketItem);
    baseketItem.count = baseketItem.count - 1;
    this.items[itemIndex] = baseketItem;

    this.updateBasket();
  }

  private updateBasket(){
    localStorage.setItem('basketItems', JSON.stringify(this.items));
    this.basketItems.next(this.items);
  }
}
