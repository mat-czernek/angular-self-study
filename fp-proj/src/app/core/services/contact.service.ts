import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserMessage } from 'src/app/models/UserMessage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private apiBaseUrl = environment.baseUrl;

  isMessageSend: boolean = false;

  constructor(private httpClient: HttpClient) { }

  send(message: UserMessage) {

    this.httpClient
      .post<UserMessage>(`${this.apiBaseUrl}/contact`, message)
      .subscribe(
        complete => console.log('Message send!'),
        error => console.log('Could not send message.', error)
      );
  }
}
