import { Injectable } from '@angular/core';
import { authCodeFlowConfig } from '../auth.config';
import { OAuthService } from 'angular-oauth2-oidc';
import { BehaviorSubject } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private _isAuthenticated = new BehaviorSubject<boolean>(false);
  public isAuthenticated$ = this._isAuthenticated.asObservable();

  constructor(private oauthService: OAuthService) {
    this.bootstrapAuthentication();

    this.oauthService.events.subscribe(_ => {
      this._isAuthenticated.next(this.oauthService.hasValidAccessToken());
    });

    this.oauthService.events
      .pipe(filter(e => ['token_received'].includes(e.type)))
      .subscribe(e => this.oauthService.loadUserProfile());
  }

  bootstrapAuthentication(): void{
    this.oauthService.configure(authCodeFlowConfig);
    this.oauthService.setStorage(sessionStorage);
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  login() {
    this.oauthService.initLoginFlow();
  }

  logout(): void{
    this.oauthService.revokeTokenAndLogout({
      client_id: this.oauthService.clientId
    }, false);
  }

  get givenName() {
    const claims = this.oauthService.getIdentityClaims();
    if (!claims) {
      return null;
    }

    return this.claimsToMap(claims).get('name');
  }

  private claimsToMap(claims: object): Map<string,string>{
    if(claims != null){
      return new Map<string, string>(Object.entries(claims));
    }
    else{
      return new Map<string, string>();
    }
  }
}
