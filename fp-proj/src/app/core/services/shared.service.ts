import { Injectable } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { BasketService } from './basket.service';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(private basketService: BasketService, private productService: ProductService) { }

  addItemToBasket(product: Product){
    this.basketService.insertBasketItem(product);
  }

  increaseProductStockQuantity(product: Product){
    this.productService.increaseProductStockQuantity(product);
  }

  decreaseProductStockQuantity(product: Product){
    this.productService.decreaseProductStockQuantity(product);
  }

  isProductInStock(product: Product): boolean{
    if(product.stockQuantity == null){
      return false;
    }

    if(this.basketService.getProductsCountById(product.id) == product.stockQuantity){
      return false;
    }

    return product.stockQuantity > 0;
  }
}
