import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { UserMessage } from 'src/app/models/UserMessage';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private readonly _apiBaseUrl = environment.baseUrl;

  private _messages = new BehaviorSubject<UserMessage[]>([]);
  readonly messages$ = this._messages.asObservable();

  constructor(private httpClient: HttpClient) { }

  fetchMessages() {
    this.httpClient.get(`${this._apiBaseUrl}/contact`, { observe: 'response' }).subscribe(
      data => {
        let messages = data.body as UserMessage[];
        this._messages.next(messages);
      },
      error => console.log('Could not load messages.', error)
    );
  }
}
