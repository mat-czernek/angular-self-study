import { Injectable } from '@angular/core';
import { BehaviorSubject, interval, Observable, Subscription } from 'rxjs';

import { Product } from 'src/app/models/Product';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private readonly _apiBaseUrl = environment.baseUrl;

  private _products = new BehaviorSubject<Product[]>([]);
  readonly products$ = this._products.asObservable();

  private _totalProducts = new BehaviorSubject<number>(0);
  readonly totalProducts$ = this._totalProducts.asObservable();

  private _radnomProducts = new BehaviorSubject<Product[]>([]);
  readonly randomProducts$ = this._radnomProducts.asObservable();

  private _randomProductsSubscription: Subscription | undefined;

  constructor(private httpClient: HttpClient) {
    this._randomProductsSubscription = interval(environment.randomProductsInterval).subscribe(() => this.fetchRandomProducts());
  }

  getProduct(productId: number): Observable<Product> {
    return this.httpClient.get<Product>(`${this._apiBaseUrl}/products/${productId}`);
  }

  updateProduct(product: Product): void {
    this.httpClient.put<Product>(`${this._apiBaseUrl}/products/${product.id}`, product).subscribe(error => console.log('Could not update the product.', error));
  }

  increaseProductStockQuantity(product: Product){
    if(!product.stockQuantity){
      product.stockQuantity = 1;
    }
    else{
      product.stockQuantity++;
    }

    this.httpClient.put<Product>(`${this._apiBaseUrl}/products/${product.id}`, product).subscribe(error => console.log('Could not update the product stock quantity.', error));
  }

  decreaseProductStockQuantity(product: Product){
    if(product.stockQuantity == 0){
      return;
    }

    if(!product.stockQuantity){
      product.stockQuantity = 0;
    }
    else{
      product.stockQuantity--;
    }

    this.httpClient.put<Product>(`${this._apiBaseUrl}/products/${product.id}`, product).subscribe(error => console.log('Could not update the product stock quantity.', error));
  }

  addProduct(product: Product): void {
    this.httpClient.post<Product>(`${this._apiBaseUrl}/products`, product).subscribe(error => console.log('Could not add product.', error));
  }

  fetchProducts(pageNumber: number = 1, limitPerPage: number = 4){

    const params = new HttpParams()
    .set(environment.httpParams.pageLimit, pageNumber)
    .set(environment.httpParams.limit, limitPerPage);

    this._fetchProductsByParams(params);
  }

  filterProducts(term: string, pageNumber: number = 1, limitPerPage: number = 4) {

    const params = new HttpParams()
    .set(environment.httpParams.pageLimit, pageNumber)
    .set(environment.httpParams.limit, limitPerPage)
    .set(environment.httpParams.searchType, term);

    this._fetchProductsByParams(params);
  }

  fetchRandomProducts(): void{
    this.httpClient.get(`${this._apiBaseUrl}/products`).subscribe(
      data => {
        this._radnomProducts.next(this._getRandomProducts(data as Product[], environment.numberOfRandomProductsOnHomePage));
        console.log(this._radnomProducts.value);
      },
    );
  }

  ngOnDestory(){
    this._randomProductsSubscription?.unsubscribe();
  }

  private _fetchProductsByParams(params: HttpParams){
    this.httpClient.get(`${this._apiBaseUrl}/products`, { params, observe: 'response' }).subscribe(
      data => {
        let products = data.body as Product[];
        this._products.next(products);
        this._totalProducts.next(parseInt(data.headers.get('x-total-count') as string));
      },
      error => console.log('Could not load products.', error)
    );
  }

  private _getRandomProducts(products: Product[], amount: number): Product[]{
    return products.sort(() => .5 - Math.random()).slice(0, amount)
  }
}
