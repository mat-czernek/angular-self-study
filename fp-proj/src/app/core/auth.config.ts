import { AuthConfig } from 'angular-oauth2-oidc';
import { environment } from 'src/environments/environment';

export const authCodeFlowConfig: AuthConfig = {
  issuer: environment.oauthServiceConfig.issuer,
  revocationEndpoint: environment.oauthServiceConfig.revocationEndpoint,
  redirectUri: environment.oauthServiceConfig.redirectUri,
  logoutUrl: environment.oauthServiceConfig.logoutUrl,
  clientId: environment.oauthServiceConfig.clientId,
  responseType: 'code',
  scope: 'openid profile email api offline_access',
  showDebugInformation: environment.oauthServiceConfig.debugInfo
};
