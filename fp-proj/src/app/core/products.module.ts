import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProductsListComponent } from '../products-list/products-list.component';
import { ProductDetailsComponent } from '../product-details/product-details.component';
import { ProductsRoutingModule } from '../products-routing/products-routing.module';
import { ProductFilterComponent } from '../product-filter/product-filter.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';


@NgModule({
  declarations: [
    ProductsListComponent,
    ProductDetailsComponent,
    ProductFilterComponent
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    PaginationModule
  ]
})
export class ProductsModule { }
