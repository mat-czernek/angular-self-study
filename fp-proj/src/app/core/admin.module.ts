import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminDahsboardComponent } from '../admin-dahsboard/admin-dahsboard.component';
import { AdminMessagesComponent } from '../admin-messages/admin-messages.component';
import { AdminProductFormComponent } from '../admin-product-form/admin-product-form.component';
import { AdminProductAddComponent } from '../admin-product-add/admin-product-add.component';
import { AdminProductEditComponent } from '../admin-product-edit/admin-product-edit.component';
import { ProductsRoutingModule } from '../products-routing/products-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from './shared.module';
import { AdminRoutingModule } from '../admin-routing/admin-routing.module';


@NgModule({
  declarations: [
    AdminDahsboardComponent,
    AdminMessagesComponent,
    AdminProductFormComponent,
    AdminProductAddComponent,
    AdminProductEditComponent,
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class AdminModule { }
