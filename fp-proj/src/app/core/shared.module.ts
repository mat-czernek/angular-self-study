import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormValidatorComponent } from '../form-validator/form-validator.component';



@NgModule({
  declarations: [
    FormValidatorComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    FormValidatorComponent
  ]
})
export class SharedModule { }
