import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { AuthenticationService } from '../core/services/authentication.service';
import { BasketService } from '../core/services/basket.service';

import { ProductService } from '../core/services/product.service';
import { SharedService } from '../core/services/shared.service';
import { Product } from '../models/Product';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  readonly isAuthenticated$: Observable<boolean> = this.authenticationService.isAuthenticated$.pipe(shareReplay());

  product: Product | undefined;

  constructor(
    private route: ActivatedRoute,
    private productService: ProductService,
    private authenticationService: AuthenticationService,
    public sharedService: SharedService
  ) { }

  ngOnInit(): void {
    this.productService.getProduct(this.getProductIdFromRouteSnapshot()).subscribe(
      ((data: Product) => this.product = { ...data})
    );
  }

  getProductIdFromRouteSnapshot(): number{
    return Number(this.route.snapshot.paramMap.get('id'));
  }
}
