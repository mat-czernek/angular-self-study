import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { ValidationErrors } from '@angular/forms';

@Component({
  selector: 'app-form-validator',
  templateUrl: './form-validator.component.html',
  styleUrls: ['./form-validator.component.scss']
})
export class FormValidatorComponent implements OnInit {

  @Input() validationErrors: ValidationErrors | null | undefined;
  @Input() errorTypeAndMessage: {[key: string]: string} = {'':''};

  formValidationMessage: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges): void {

    let currentValidationErrors = changes.validationErrors.currentValue;

    if(currentValidationErrors != null && currentValidationErrors != changes.validationErrors.previousValue){

        Object.keys(currentValidationErrors).forEach(keyError => {
          this.formValidationMessage = this.errorTypeAndMessage[keyError];
        });

    }

  }
}
