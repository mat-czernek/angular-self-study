import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDahsboardComponent } from './admin-dahsboard.component';

describe('AdminDahsboardComponent', () => {
  let component: AdminDahsboardComponent;
  let fixture: ComponentFixture<AdminDahsboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminDahsboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDahsboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
